/*
 * Copyright (C) 2018  Israel Felipe Prates
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "bst.h"

int main(void) {
	/* Initialize the nodes. */
	node_t a = {NULL, NULL, 'A'};
	node_t b = {NULL, NULL, 'B'};
	node_t c = {NULL, NULL, 'C'};
	node_t d = {NULL, NULL, 'D'};
	node_t e = {NULL, NULL, 'E'};
	node_t f = {NULL, NULL, 'F'};
	node_t g = {NULL, NULL, 'G'};
	node_t h = {NULL, NULL, 'H'};
	node_t i = {NULL, NULL, 'I'};
	node_t j = {NULL, NULL, 'J'};

	/*
	 * Visual representation:
	 *
	 *     D
	 *    / \
	 *   B   E
	 *  / \   \
	 * A   C   F
	 *          \
	 *           J
	 *          /
	 *         H
	 *        / \
	 *       G   I
	 */

	/* Set the edges. */
	d.left = &b;
	b.left = &a;
	b.right = &c;
	d.right = &e;
	e.right = &f;
	f.right = &j;
	j.left = &h;
	h.left = &g;
	h.right = &i;

	/* Traversals. */
	printf("Preorder:\n");
	bst_traverse_preorder(&d);
	printf("Inorder:\n");
	bst_traverse_inorder(&d);
	printf("Postorder:\n");
	bst_traverse_postorder(&d);

	return 0;
}
