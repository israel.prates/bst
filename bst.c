/*
 * Copyright (C) 2018  Israel Felipe Prates
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "bst.h"

void bst_traverse_preorder(node_t *root) {
	if (root != NULL) {
		printf("%c\n", root->data);
		bst_traverse_preorder(root->left);
		bst_traverse_preorder(root->right);
	}
}

void bst_traverse_inorder(node_t *root) {
	if (root != NULL) {
		bst_traverse_inorder(root->left);
		printf("%c\n", root->data);
		bst_traverse_inorder(root->right);
	}
}

void bst_traverse_postorder(node_t *root) {
	if (root != NULL) {
		bst_traverse_postorder(root->left);
		bst_traverse_postorder(root->right);
		printf("%c\n", root->data);
	}
}
